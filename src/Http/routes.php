<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::group([
    'namespace'  =>'Serhii81\Mytestblog\Http\Controllers',
    'middleware' => 'web',
    'domain' => config('url'),
    'prefix' => 'mytestblog',
    ], function() {
    
    Route::get('/', 'PostController@index')->name('post.index')->middleware('auth');
    Route::get('/create_post_form', 'PostController@createForm')->name('post.create_form');
    Route::post('/create', 'PostController@createPost')->name('post.create');
    Route::get('/post/{post_id}', 'PostController@showPost')->name('post.show');
    Route::get('/edit_post_form/{post_id}', 'PostController@editForm')->name('post.edit_form');
    Route::post('edit', 'PostController@editPost')->name('post.edit');
    
    
});
