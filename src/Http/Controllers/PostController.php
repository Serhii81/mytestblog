<?php

namespace Serhii81\Mytestblog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Serhii81\Mytestblog\Post;
use Serhii81\Mytestblog\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Contracts\Validation\Validator;

class PostController extends Controller
{
    use ValidatesRequests;
    /**
     * Функция вывода постов по пять на странице
     * @return type
     */
    public function index() {
        $posts = Post::orderBy('id', 'desc')->paginate(5);      
        return view('mytestblog::index', ['posts'=>$posts]);
    }
    
    /**
     * Переход к форме создания поста
     * @return type
     */
    public function createForm() {        
        return view('mytestblog::createForm');
    }
    
    /**
     * Функция сохранения нового поста
     * @param Request $request
     * @return type
     */
    public function createPost(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required',
            'picture' => 'required',
        ],
        [
            'title.required' => 'Пустой заголовок',
            'text.required' => 'Нет текста поста',           
            'picture.required' => 'нет файла',
        ]);        
        $post_title = $request->title;
        $post_text = $request->text;
        $post_short_text = mb_substr($request->text, 0, 120);
        $post_author = $request->author;
        $post_author_id = $request->user_id;
        $post_time = time();          
        if($request->hasFile('picture')){
            $picture = $request->file('picture');
            $fileName = $picture->getClientOriginalName();
            $destinationPath = public_path('/images');
            $picture->move($destinationPath, $fileName);
            $post_picture = './images/'.$fileName;
        }        
        
        $new_post = new Post();
        $new_post->title = $post_title;
        $new_post->text = $post_text;
        $new_post->short_text = $post_short_text;
        $new_post->author = $post_author;
        $new_post->user_id = $post_author_id;
        $new_post->created_at = $post_time = time();
        $new_post->url_picture = $post_picture;
        $new_post->save();
        
        return redirect()->route('post.index');
        
    }
    
    /**
     * Вывод одного поста
     * @param type $id
     * @return type
     */
    public function showPost($id){
        $post = Post::find($id);
        return view('mytestblog::showPost', ['post'=>$post]);
    }
    
    /**
     * Переход к форму редактирования выбранного поста
     * @param type $id
     * @return type
     */
    public function editForm($id) {
        $post = Post::find($id);
        return view('mytestblog::editForm', ['post'=>$post]);
    }
    
    /**
     * Функция редактирования выбранного поста
     * @param Request $request
     * @return type
     */
    public function editPost(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required',
            
        ],
        [
            'title.required' => 'Пустой заголовок',
            'text.required' => 'Нет текста поста',           
            
        ]);  
        $post_id = $request->id;
        $post_title = $request->title;
        $post_text = $request->text;
        $post_short_text = mb_substr($request->text, 0, 120);
        $post_time = time();          
        if($request->hasFile('picture')){
            $picture = $request->file('picture');
            $fileName = $picture->getClientOriginalName();
            $destinationPath = public_path('/images');
            $picture->move($destinationPath, $fileName);
            $post_picture = './images/'.$fileName;
        }else{
            $post_picture = $request->old_picture;
        }
        
        $edit_post = Post::find($post_id);
        $edit_post->title = $post_title;
        $edit_post->text = $post_text;
        $edit_post->short_text = $post_short_text;
        $edit_post->updated_at = $post_time = time();
        $edit_post->url_picture = $post_picture;
        $edit_post->save();
        
        return redirect()->route('post.index');
    }
}
