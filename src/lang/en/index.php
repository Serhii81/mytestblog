<?php

return [
    'create' => 'Create post',
    'all' => 'All posts',
    'author' => 'Author',
    'datePublish' => 'Publication date',
    'dateEdit' => 'Edit date',
    'full' => 'Full version',
    'edit' => 'Edit',
    'return' => 'Return to posts',
    'no' => 'No posts',
];

