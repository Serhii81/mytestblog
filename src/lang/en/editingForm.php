<?php

return [
    'editing' => 'Editing post',
    'title' => 'Title',
    'text' => 'Edit text',
    'picture' => 'Edit picture',
    'edit' => 'Edit post',
];

