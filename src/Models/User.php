<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Serhii81\Mytestblog;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;



/**
 * Description of User
 *
 * @author Сергій
 */
class User extends App\User{
    use HasRoleAndPermission;
    
    protected $table = 'user';
    /**
     * Функция нахождения постов админа
     * (условие о невозможности редактирования постов админа пользователя с ролью editor)
     * @param type $id
     * @return type
     */
    public function adminPost($id){
        $user = User::find($id);
        if($user->hasRole('admin')){
            return $user->id;
        }
    }
}
