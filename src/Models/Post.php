<?php

namespace Serhii81\Mytestblog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'text', 'short_text', 'url_picture'];
    
}
