@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <h4>@lang('mytestblog::creatingForm.adding')</h4>
        </div>
        <div class="col-md-9">
            <form style="margin-top: 20px" action="{{route('post.create')}}" method="POST" name="form" enctype="multipart/form-data" id="form">
                {{ csrf_field() }}
                
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="author" value="{{Auth::user()->name}}">
                <div class="form-group">
                  <label for="title">@lang('mytestblog::creatingForm.title')</label>
                  <input style="margin-left: 10px" type="text" name="title" id="title" placeholder="Заголовок" required="">
                </div>
                <div class="form-group">
                    <div><label for="text">@lang('mytestblog::creatingForm.text')</label></div>
                  <textarea rows="10" cols="45" id="text" name='text' required=""></textarea>                  
                </div>
                <div class="form-group">
                  <label for="picture">@lang('mytestblog::creatingForm.pictures')</label>
                  <input type="file" id="picture" name='picture' required="">                  
                </div>
                <input type="submit" class="btn btn-primary" value="@lang('mytestblog::creatingForm.add')">
              </form>
            
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
            @endif
        </div>        
    </div>
</div>

@endsection