@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <h4>@lang('mytestblog::editingForm.editing')</h4>
        </div>
        <div class="col-md-9">
            <form style="margin-top: 20px" action="{{route('post.edit')}}" method="POST" name="form" enctype="multipart/form-data" id="form">
                {{ csrf_field() }}
                
                
                <div class="form-group">
                  <input type="hidden" name="id"  value='{{$post->id}}'>
                  <label for="title">@lang('mytestblog::editingForm.title')</label>
                  <input style="margin-left: 10px" type="text" name="title" id="title" value="{{ $post->title }}"  required="">
                </div>
                <div class="form-group">
                    <div><label for="text">@lang('mytestblog::editingForm.text')</label></div>
                  <textarea rows="10" cols="45" id="text" name='text' required="">{{ $post->text }}</textarea>                  
                </div>
                <div class="form-group">
                  <label for="picture">@lang('mytestblog::editingForm.picture')</label>
                  <input type="file" id="picture" name='picture'>     
                  <input type="hidden" name="old_picture"  value='{{$post->url_picture}}'>
                </div>
                <input type="submit" class="btn btn-primary" value="@lang('mytestblog::editingForm.edit')">
            </form>
            
            
            @if (count($errors) > 0)                                        
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
            @endif            
        </div>        
    </div>
</div>

@endsection