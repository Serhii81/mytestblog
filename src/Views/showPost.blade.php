@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="{{route('post.create_form')}}" class="btn btn-info">@lang('mytestblog::index.create')</a>
            </div>
        </div>
        <div class="row">                                  
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-preview">
                    <h2 class="post-title">{{$post->title}}</h2>
                    <p class="post-meta">@lang('mytestblog::index.author'): {{$post->author}}</p>                  
                    <p class="post-meta">@lang('mytestblog::index.datePublish'): {{$post->created_at}}</p>
                    @if($post->created_at != $post->updated_at)
                        <p class="post-meta">@lang('mytestblog::index.dateEdit'): {{$post->updated_at}}</p> 
                    @endif
                    <img src="../.{{$post->url_picture}}" alt="{{$post->title}}" class="img-thumbnail" width="200px" height="200px">                            
                    <h5 class="post-subtitle">{{$post->text}}</h5>
                    @if(Auth::user()->id == $post->user_id || Auth::user()->hasRole(['admin']))
                        <a href="{{route('post.edit_form', $post->id)}}" class="btn btn-info">@lang('mytestblog::index.edit')</a>
                        @elseif(Auth::user()->hasRole(['editor']) && $post->user_id != Auth::user()->adminPost($post->user_id))
                        <a href="{{route('post.edit_form', $post->id)}}" class="btn btn-info">@lang('mytestblog::index.edit')</a>
                    @endif
                    <hr>                     
                    <a href="{{route('post.index')}}" class="btn btn-info">@lang('mytestblog::index.return')</a>                    
                </div>
            </div>   
        </div>
    </div>
@endsection
