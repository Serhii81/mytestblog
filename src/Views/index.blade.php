@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="{{route('post.create_form')}}" class="btn btn-info">@lang('mytestblog::index.create')</a>
            </div>
        </div>
        <div class="row">                                  
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="post-preview">
                        <h2 style="border-bottom: 1px solid gray">@lang('mytestblog::index.all')</h2>
                        @if (!empty($posts))                        
                            @foreach ($posts as $post)                                    
                                <h2 class="post-title">
                                    <a href="{{route('post.show', $post->id)}}">{{$post->title}}</a>
                                </h2>
                                <p class="post-meta">@lang('mytestblog::index.author'): {{$post->author}}</p>                  
                                <p class="post-meta">@lang('mytestblog::index.datePublish'): {{$post->created_at}}</p>
                                @if($post->created_at != $post->updated_at)
                                   <p class="post-meta">@lang('mytestblog::index.dateEdit'): {{$post->updated_at}}</p> 
                                @endif                                
                                <img src="{{$post->url_picture}}" alt="{{$post->title}}" class="img-thumbnail" width="200px" height="200px"> 
                                <h5 class="post-subtitle">
                                    @if(strlen($post->text) > 120)
                                        {{$post->short_text}}...
                                    @else
                                        {{$post->text}}
                                    @endif                                    
                                </h5>
                                <a href="{{route('post.show', $post->id)}}" class="btn btn-info">@lang('mytestblog::index.full')</a>
                                @if(Auth::user()->id == $post->user_id || Auth::user()->hasRole(['admin']))
                                    <a href="{{route('post.edit_form', $post->id)}}" class="btn btn-info">@lang('mytestblog::index.edit')</a>
                                    @elseif(Auth::user()->hasRole(['editor']) && $post->user_id != Auth::user()->adminPost($post->user_id))
                                        <a href="{{route('post.edit_form', $post->id)}}" class="btn btn-info">@lang('mytestblog::index.edit')</a>
                                @endif
                            <hr>
                            @endforeach
                            <div>
                                {{$posts->links()}}
                            </div>
                        @else
                            <h3>No records</h3>                             
                        @endif                    
                    </div>
                </div>                
            
        </div>
    </div>
@endsection