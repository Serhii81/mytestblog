<?php

namespace Serhii81\Mytestblog;

use Illuminate\Support\ServiceProvider;

class MytestblogServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /*
        if ($this->app->runningInConsole()) {
            $this->commands([
                MytestblogInstall::class,   // php artisan mytestblog:install
            ]);
        }*/
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
        //$this->loadMigrationsFrom(__DIR__.'/../migrations');
        $this->loadViewsFrom(__DIR__.'/Views', 'mytestblog');
        $this->loadTranslationsFrom(__DIR__.'/lang', 'mytestblog');

        $this->publishes([
          __DIR__.'/mytestblog/lang' => resource_path('lang/vendor/mytestblog/lang'),    
        ], 'lang');        
        
        $this->publishes([
            __DIR__.'/config/mytestblog.php' => config_path('mytestblog.php'),
        ], 'config');
        
        
        $this->publishes([
            __DIR__ . '/../migrations/' => base_path('/database/migrations'),
        ], 'migrate');
		

        $this->publishes([
            __DIR__ . '/../seeds/' => base_path('/database/seeds'),
        ], 'seeds');
        
    }
}
