<?php

namespace Serhii81\Mytestblog\Console\Commands;

use Illuminate\Console\Command;

class MytestblogInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mytestblog:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Up migrations and Run seeds in Mytestblog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate', ['--path'=>'Serhii81\Mytestblog\migrations']);
        
        $this->call('db:seed');
    }
}
