<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'id' => 1,
            'name'=> 'admin',
            'email' => 'admin@gmail.com',            
            'password' => Hash::make('12345678')                              
            ],
            [            
            'id' => 2,
            'name'=> 'editor',
            'email' => 'editor@gmail.com',
            'password' => Hash::make('12345678')
            ],
            [
            'id' => 3,
            'name'=> 'author1',
            'email' => 'author1@gmail.com',            
            'password' => Hash::make('12345678')                               
            ],
            [
            'id' => 4,
            'name'=> 'author2',
            'email' => 'author2@gmail.com',            
            'password' => Hash::make('12345678')                               
            ],
            [
            'id' => 5,
            'name'=> 'author3',
            'email' => 'author3@gmail.com',            
            'password' => Hash::make('12345678')                               
            ],
        ]);
    }
}
